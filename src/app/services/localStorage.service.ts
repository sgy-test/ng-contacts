import { Injectable } from '@angular/core';
import { IEntity } from '../common/entity';
import { UUID } from 'angular2-uuid';

@Injectable()
export class LocalStorageService {

    saveObject<T extends IEntity>(key: string, object: T) {
        const objects = this.getObjectList(key);

        if (!object.id) {
            object.id = UUID.UUID();
            objects.push(object);
        } else {
            const index = objects.findIndex((o: IEntity) => o.id === object.id);
            objects[index] = object;
        }

        this.saveObjectList<T>(key, objects);
    }

    saveObjectList<T>(key: string, objects: object[]) {
        localStorage.setItem(key, JSON.stringify(objects));
    }

    getObjectList<T>(key: string): T[] {
        const objects = JSON.parse(localStorage.getItem(key));
        return objects || [];
    }
}
