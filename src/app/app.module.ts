import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import {
    AppComponent,
    ContactListComponent,
    ContactEditComponent,
    ContactElementComponent,
    ContactService,
    ContactFactory
} from './components/index';

import {
    LocalStorageService
} from './services/index';

import { appRoutes } from './routes';

@NgModule({
    declarations: [
        AppComponent,
        ContactListComponent,
        ContactEditComponent,
        ContactElementComponent
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot(appRoutes),
        FormsModule,
        NgbModule.forRoot()
    ],
    providers: [
        LocalStorageService,
        ContactService,
        ContactFactory
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
