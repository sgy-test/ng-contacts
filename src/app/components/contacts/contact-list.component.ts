import { Component } from '@angular/core';
import { IContact } from './contact.model';
import { ContactService } from './shared/contact.service';

@Component({
    templateUrl: 'contact-list.component.html'
})

export class ContactListComponent {

    constructor(private contactService: ContactService) {}

    contacts: IContact[] = this.contactService.getAllContacts();

    addContact(contact) {
        this.contactService.addContact(contact);
    }
}
