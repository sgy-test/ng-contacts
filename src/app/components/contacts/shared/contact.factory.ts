import { Injectable } from '@angular/core';
import { IContact } from '../contact.model';

@Injectable()
export class ContactFactory {
    create(): IContact {
        return {
            id: '',
            firstname: '',
            lastname: '',
            age: 0
        };
    }
}
