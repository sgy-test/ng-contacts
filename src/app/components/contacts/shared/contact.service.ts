import { Injectable } from '@angular/core';
import { IContact } from '../contact.model';
import { LocalStorageService } from '../../../services';

@Injectable()
export class ContactService {

    readonly ENTITYKEY = 'contacts';

    constructor(private localStorageService: LocalStorageService) {}

    addContact(contact: IContact) {
        this.localStorageService.saveObject(this.ENTITYKEY, contact);
    }

    getAllContacts(): IContact[] {
        return this.localStorageService.getObjectList(this.ENTITYKEY);
    }

    getContact(id: string): IContact {
        const contacts = this.getAllContacts();
        return contacts.find(contact => contact.id === id);
    }
}
