import { Component, Input } from '@angular/core';
import { IContact } from './contact.model';

@Component({
    selector: '[app-contact-element]',
    templateUrl: 'contact-element.component.html'
})

export class ContactElementComponent {
    @Input() contact: IContact;
}
