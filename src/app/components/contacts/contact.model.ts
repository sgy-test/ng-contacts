import { IEntity } from '../../common/entity';

export class IContact implements IEntity {
    id: string;
    firstname: string;
    lastname: string;
    age: number;
}
