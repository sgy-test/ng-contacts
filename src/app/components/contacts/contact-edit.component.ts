import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { IContact } from './contact.model';
import { ContactService } from './shared/contact.service';
import { ContactFactory } from './shared/contact.factory';

@Component({
    templateUrl: 'contact-edit.component.html'
})

export class ContactEditComponent implements OnInit {
    contact: IContact;

    constructor(
        private contactService: ContactService,
        private contactFactory: ContactFactory,
        private router: Router,
        private route: ActivatedRoute
    ) {}

    ngOnInit() {
        const contact = this.contactService.getContact(this.route.snapshot.params['id']);
        this.contact = contact || this.contactFactory.create();
    }

    save() {
        this.contactService.addContact(this.contact);
        this.router.navigate(['/contacts']);
    }
}
