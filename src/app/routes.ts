import { Routes } from '@angular/router';

import {
    AppComponent,
    ContactEditComponent,
    ContactListComponent
} from './components/index';

export const appRoutes: Routes = [
    { path: 'contacts', component: ContactListComponent },
    { path: 'contacts/add', component: ContactEditComponent },
    { path: 'contacts/:id', component: ContactEditComponent },
    { path: '', redirectTo: 'contacts', pathMatch: 'full'},
    { path: '**', redirectTo: 'contacts', pathMatch: 'full'}
];
